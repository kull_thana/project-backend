package camt.se234.project.service;

import camt.se234.project.dao.UserDao;
import camt.se234.project.entity.User;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthenticationServiceImplTest {
    UserDao userDao;
    AuthenticationServiceImpl authenticationService;

    @Before
    public void setUserDaoService() {
        userDao = mock(UserDao.class);
        authenticationService = new AuthenticationServiceImpl();
        authenticationService.setUserDao(userDao);
    }

    @Test
    public void testAuthenticateExistUser() {
        User user1 = User.builder().id(001L).username("kull").password("0123").role("admin").build();
        when(userDao.getUser("kull","0123")).thenReturn(user1);
        User user2 = User.builder().id(101L).username("cha").password("4567").role("user").build();
        when(userDao.getUser("cha","4567")).thenReturn(user2);
        assertThat(authenticationService.authenticate("kull","0123"),is(user1));
        assertThat(authenticationService.authenticate("cha","4567"),is(user2));
    }

    @Test
    public void testAuthenticateNotExistUser() {
        User user1 = User.builder().id(001L).username("kull").password("0123").role("admin").build();
        when(userDao.getUser("kull","0123")).thenReturn(user1);
        User user2 = User.builder().id(101L).username("cha").password("4567").role("user").build();
        when(userDao.getUser("cha","4567")).thenReturn(user2);
        assertThat(authenticationService.authenticate("john","doe"),nullValue());
    }
}
