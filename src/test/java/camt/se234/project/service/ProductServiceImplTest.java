package camt.se234.project.service;

import camt.se234.project.dao.ProductDao;
import camt.se234.project.entity.Product;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ProductServiceImplTest {
    ProductDao productDao;
    ProductServiceImpl productService;

    @Before
    public void setProductDaoService(){
        productDao = mock(ProductDao.class);
        productService = new ProductServiceImpl();
        productService.setProductDao(productDao);
    }

    @Test
    public void testGetAllProducts(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(501L).productId("B101").name("Taokaenoi").price(45.00).description("The largest seaweed manufacturer in Thailand").imageLocation(null).build());
        mockProducts.add(Product.builder().id(502L).productId("B201").name("MAMA Tomyumkung").price(51.00).description("Tom Yum Flavour Instant Noodles (Shrimp Flavored)").imageLocation(null).build());
        mockProducts.add(Product.builder().id(503L).productId("B301").name("M150").price(-15.00).description("Energy drink contains caffeine").imageLocation(null).build());
        mockProducts.add(Product.builder().id(504L).productId("B401").name("DUCK(Ped Pro)").price(69.00).description("Bathroom cleaning liquid Concentrated formula kill bacteria up to 99.9%").imageLocation(null).build());
        when(productDao.getProducts()).thenReturn(mockProducts);
        assertThat(productService.getAllProducts(), contains(mockProducts.toArray()));
    }

    @Test
    public void testGetAvailableProducts(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(501L).productId("B101").name("Taokaenoi").price(45.00).description("The largest seaweed manufacturer in Thailand").imageLocation(null).build());
        mockProducts.add(Product.builder().id(502L).productId("B201").name("MAMA Tomyumkung").price(51.00).description("Tom Yum Flavour Instant Noodles (Shrimp Flavored)").imageLocation(null).build());
        mockProducts.add(Product.builder().id(503L).productId("B301").name("M150").price(-15.00).description("Energy drink contains caffeine").imageLocation(null).build());
        mockProducts.add(Product.builder().id(504L).productId("B401").name("DUCK(Ped Pro)").price(69.00).description("Bathroom cleaning liquid Concentrated formula kill bacteria up to 99.9%").imageLocation(null).build());
        when(productDao.getProducts()).thenReturn(mockProducts);
        assertThat(productService.getAvailableProducts(), hasItems(
                Product.builder().id(501L).productId("B101").name("Taokaenoi").price(45.00).description("The largest seaweed manufacturer in Thailand").imageLocation(null).build(),
                Product.builder().id(502L).productId("B201").name("MAMA Tomyumkung").price(51.00).description("Tom Yum Flavour Instant Noodles (Shrimp Flavored)").imageLocation(null).build(),
                Product.builder().id(504L).productId("B401").name("DUCK(Ped Pro)").price(69.00).description("Bathroom cleaning liquid Concentrated formula kill bacteria up to 99.9%").imageLocation(null).build()
        ));
    }

    @Test
    public void testGetUnavailableProductSize(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(501L).productId("B101").name("Taokaenoi").price(45.00).description("The largest seaweed manufacturer in Thailand").imageLocation(null).build());
        mockProducts.add(Product.builder().id(502L).productId("B201").name("MAMA Tomyumkung").price(51.00).description("Tom Yum Flavour Instant Noodles (Shrimp Flavored)").imageLocation(null).build());
        mockProducts.add(Product.builder().id(503L).productId("B301").name("M150").price(-15.00).description("Energy drink contains caffeine").imageLocation(null).build());
        mockProducts.add(Product.builder().id(504L).productId("B401").name("DUCK(Ped Pro)").price(69.00).description("Bathroom cleaning liquid Concentrated formula kill bacteria up to 99.9%").imageLocation(null).build());
        when(productDao.getProducts()).thenReturn(mockProducts);
        assertThat(productService.getUnavailableProductSize(),is(1));
    }
}
