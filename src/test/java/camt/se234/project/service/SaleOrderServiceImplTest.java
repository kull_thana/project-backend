package camt.se234.project.service;

import camt.se234.project.dao.OrderDao;
import camt.se234.project.entity.Product;
import camt.se234.project.entity.SaleOrder;
import camt.se234.project.entity.SaleTransaction;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SaleOrderServiceImplTest {
    OrderDao orderDao;
    SaleOrderServiceImpl saleOrderService;
    static List<SaleOrder> mockSaleOrders;

    @Before
    public void setOrderDaoService() {
        orderDao = mock(OrderDao.class);
        saleOrderService = new SaleOrderServiceImpl();
        saleOrderService.setOrderDao(orderDao);
        when(orderDao.getOrders()).thenReturn(mockSaleOrders);
    }

    @BeforeClass
    public static void setData(){
        List<Product> mockProducts = new ArrayList<>();
        mockProducts.add(Product.builder().id(501L).productId("B101").name("Taokaenoi").price(45.00).description("The largest seaweed manufacturer in Thailand").imageLocation(null).build());
        mockProducts.add(Product.builder().id(502L).productId("B201").name("MAMA Tomyumkung").price(51.00).description("Tom Yum Flavour Instant Noodles (Shrimp Flavored)").imageLocation(null).build());
        mockProducts.add(Product.builder().id(503L).productId("B301").name("M150").price(-15.00).description("Energy drink contains caffeine").imageLocation(null).build());
        mockProducts.add(Product.builder().id(504L).productId("B401").name("DUCK(Ped Pro)").price(69.00).description("Bathroom cleaning liquid Concentrated formula kill bacteria up to 99.9%").imageLocation(null).build());

        List<SaleTransaction> mockSaleTransactions = new ArrayList<>();
        mockSaleTransactions.add(SaleTransaction.builder().id(600L).transactionId("123").product(mockProducts.get(0)).amount(10).build());
        mockSaleTransactions.add(SaleTransaction.builder().id(700L).transactionId("456").product(mockProducts.get(1)).amount(20).build());
        mockSaleTransactions.add(SaleTransaction.builder().id(800L).transactionId("789").product(mockProducts.get(2)).amount(30).build());
        mockSaleTransactions.add(SaleTransaction.builder().id(900L).transactionId("101").product(mockProducts.get(3)).amount(40).build());

        mockSaleOrders = new ArrayList<>();
        mockSaleOrders.add(SaleOrder.builder().id(950L).saleOrderId("001").transactions(new ArrayList<SaleTransaction>() {{ add(mockSaleTransactions.get(0));add(mockSaleTransactions.get(0)); }}).build());
        mockSaleOrders.add(SaleOrder.builder().id(960L).saleOrderId("002").transactions(new ArrayList<SaleTransaction>() {{ add(mockSaleTransactions.get(1));add(mockSaleTransactions.get(3)); }}).build());
        mockSaleOrders.add(SaleOrder.builder().id(970L).saleOrderId("003").transactions(new ArrayList<SaleTransaction>() {{ add(mockSaleTransactions.get(2));add(mockSaleTransactions.get(2)); }}).build());
        mockSaleOrders.add(SaleOrder.builder().id(980L).saleOrderId("004").transactions(new ArrayList<SaleTransaction>() {{ add(mockSaleTransactions.get(3));add(mockSaleTransactions.get(1)); }}).build());
    }

    @Test
    public void testGetSaleOrders(){
        assertThat(saleOrderService.getSaleOrders(),hasItems(
                mockSaleOrders.get(0),
                mockSaleOrders.get(1),
                mockSaleOrders.get(2),
                mockSaleOrders.get(3)
        ));
    }

    @Test
    public void testGetAverageSaleOrderPrice(){
        assertThat(saleOrderService.getAverageSaleOrderPrice(),is(1890.0));
    }
}
